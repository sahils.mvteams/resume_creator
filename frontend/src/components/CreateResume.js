import React, { useState,useEffect } from 'react';
import PersonalInfo from './PersonalInfo';
import EducationalInfo from './EducationalInfo';
import ExperienceInfo from './ExperienceInfo'
import {Col,Row} from 'react-bootstrap'
import { openNotification } from '../utils/index'
import { useNavigate } from 'react-router-dom';
import axios from 'axios'
function CreateResume({edit,dataToUpdate}) {
  console.log("edit,dataToUpdate",edit,dataToUpdate)
  const navigate = useNavigate()
  const [activeTab, setActiveTab] = useState(1)
  const [listOfInstitutes, setListOfInstitutes] = useState([])
  const [commonError, setCommonError] = useState()
  const [submit, setSubmit] = useState(false);

  useEffect(()=>{

    (async()=>{
      try {
        const response = await axios.get('http://localhost:4000/api/user/fetchColleges');
        console.log("all data",response.data.colleges);
        let allData = response.data.colleges
        let arr = []
        for (var k in allData){
          let college = allData[k]
          arr.push(college.college_name)
        }
        setListOfInstitutes(arr)
      } catch (error) {
        console.error(error);
      }
    })()

  },[])
  let initialCvInfo = [{
    PersonalInformation : {},
    EducationalInformation : {},
    ExperienceInformation : {}
  }]

  const [cvInfo, setCvInfo] = useState(initialCvInfo)
  const initialValues={
    name:edit ? dataToUpdate.name : '',
    email:edit ? dataToUpdate.email : '',
    gender:edit ? dataToUpdate.gender : '',
    phoneNumber:edit ? dataToUpdate.phone : '',
    dateOfBirth: edit ? new Date(dataToUpdate.dob) : new Date()

  }
  const [personalInfo, setPersonalInfo] = useState(initialValues)

  // id	education	college_university	passing year	marks	resume_id
  const initialGraduateValues = {
    education:'graduation',
    college_uni_name :edit && dataToUpdate?.graduate_uni ? dataToUpdate?.graduate_uni  : '',
    marks:edit && dataToUpdate?.graduatemarks ? dataToUpdate?.graduatemarks : '',
    passing_year: edit && dataToUpdate?.graduate_pass_year ? new Date(dataToUpdate?.graduate_pass_year):new Date()
  }
  const initialPostGraduateValues = {
    education:'postgraduation',
    post_college_uni_name :edit && dataToUpdate?.post_graduate_uni ? dataToUpdate?.post_graduate_uni  : '',
    post_marks:edit && dataToUpdate?.post_graduatemarks ? dataToUpdate?.post_graduatemarks : '',
    post_passing_year: edit && dataToUpdate?.post_graduate_pass_year ? new Date(dataToUpdate?.post_graduate_pass_year):new Date()
  }
  const [graduateInfo, setGraduateInfo] = useState(initialGraduateValues)
  const [postgraduateInfo, setPostGraduateInfo] = useState(initialPostGraduateValues)

  // const initialexpValues={
  //   exp_years:edit ? dataToUpdate.exp_year : null,
  //   companies:edit ? [dataToUpdate.companies] : [],
  // }
  // const [experienceInfo, setExperienceInfo] = useState(initialexpValues)

  const initialexpValues = [
    {
      exp_years: edit ? dataToUpdate.exp_year : null,
      companies: edit ? [dataToUpdate.companies] : "",
    }
  ];
  const [experienceInfo, setExperienceInfo] = useState(edit ? dataToUpdate?.expInfo : initialexpValues);

  const handleChangeTab = async(e)=>{
    setActiveTab(e)
  }
  const onNext = ()=>{
    setActiveTab(prev => prev+1)
  }
  const onPrev = ()=>{
    setActiveTab(prev => prev-1)
  }

  const handleFormSubmit= async()=>{
    let createResume = await axios.post('http://localhost:4000/api/resume/create',{data:cvInfo[0]},{headers: {"x-auth-token": window.localStorage.getItem('token')}})
    console.log("createResume",createResume)
    if(createResume.data.success){
      await openNotification('Success','Resume Created Successfully')
      window.location.reload()
    }
  }

  const handleUpdateResume= async()=>{
    console.log("cvInfo", cvInfo);
    let updateResume = await axios.post(`http://localhost:4000/api/resume/update/${dataToUpdate.resume_id}`,{data:cvInfo[0]},{headers: {"x-auth-token": window.localStorage.getItem('token')}})
    console.log("updateResume",updateResume)
    if(updateResume.data.success){
      await openNotification('Success','Resume Created Successfully')
      window.location.reload()
    }
  }
  // console.log("cvInfo",cvInfo);
  return (
      <>
        <div>
          <Row>
            {/* <Col md={4} style={{textAlign:'center', border:'1px solid black', cursor:'pointer', backgroundColor:activeTab === 1 ? 'black': 'white', color: activeTab === 1 ? 'white': 'black'}} onClick={()=>handleChangeTab(1)}>
              <h3>Personal Info</h3>
            </Col>

            <Col md={4} style={{textAlign:'center', border:'1px solid black', cursor:'pointer', backgroundColor:activeTab === 2 ? 'black': 'white', color:activeTab === 2 ? 'white': 'black'}} onClick={()=>handleChangeTab(2)}>
              <h3>Educational Info</h3>
            </Col>

            <Col md={4} style={{textAlign:'center', border:'1px solid black', cursor:'pointer', backgroundColor: activeTab === 3 ? 'black': 'white', color: activeTab === 3 ? 'white': 'black'}} onClick={()=>handleChangeTab(3)}>
              <h3>Experience Info</h3>
            </Col> */}

             
              <Col md={6} style={{textAlign:'center', margin:'auto'}}  className="mt-4">
                  {
                    activeTab === 1 && <PersonalInfo cvInfo={cvInfo} setCvInfo={setCvInfo} onNext={onNext} personalInfo={personalInfo} setPersonalInfo={setPersonalInfo} setCommonError={setCommonError}/>
                  }
                  {
                    activeTab === 2 && <EducationalInfo setCvInfo={setCvInfo} onNext={onNext} onPrev={onPrev} graduateInfo={graduateInfo} setGraduateInfo={setGraduateInfo} postgraduateInfo={postgraduateInfo} setPostGraduateInfo={setPostGraduateInfo} listOfInstitutes={listOfInstitutes} setCommonError={setCommonError}/>
                  }
                  { 
                    activeTab === 3 && <ExperienceInfo setCvInfo={setCvInfo} onPrev={onPrev} experienceInfo={experienceInfo} setExperienceInfo={setExperienceInfo} handleFormSubmit={handleFormSubmit} cvInfo={cvInfo} edit={edit} handleUpdateResume={handleUpdateResume} dataToUpdate={dataToUpdate} setSubmit={setSubmit} submit={submit}/>
                  }
              </Col>
          </Row>
        </div>
      </>
      );
}   

export default CreateResume;