import React, { useEffect, useState } from 'react';
import { Card, Form, Button, Col, Row } from 'react-bootstrap';
import { Link, useAsyncError } from 'react-router-dom';
import Multiselect from 'react-widgets/Multiselect';
import Combobox from 'react-widgets/Combobox';

const listOfCompanies = ['INFOTECH ZONE', 'LN Web Work', 'Bangaree Infotech', 'W3sols', 'Promatics'];

function ExpForm({index, formIndex, handleExpChange, experienceInfo, handleExpCompanies, onRemove }) {
  console.log("index",index)
  const showCloseButton = experienceInfo.length > 1;

  return (
    <>
      <div style={{ border: '1px solid lightgrey', borderRadius: '8px' }} className="mt-2 mb-2 px-5 py-3">
        <Col md="12" style={{ textAlign: 'end' }} className="mt-2">
          {showCloseButton && <Button className="btn-close" onClick={onRemove} />}
        </Col>
        <Form.Group as={Row} className="mb-3">
          <Form.Label className="form-label text-center" column sm="2">
            Years of Experience
          </Form.Label>
          <Col sm="10" className="mt-2">
            <Form.Control
              type="number"
              placeholder="Enter Years of Exp."
              onChange={(event) => handleExpChange(event, index)}
              name="exp_years"
              id="exp_years"
              value={experienceInfo[index]?.exp_years || ''}
            />
          </Col>
        </Form.Group>

        <Form.Group className="mb-3" as={Row}>
          <Form.Label className="form-label text-center" column sm="2">
            Companies
          </Form.Label>
          <Col sm="10">
            <Combobox
              dataKey="id"
              textField="name"
              defaultValue={experienceInfo[index]?.companies?.length > 0 ? experienceInfo[index]?.companies : ""}
              data={listOfCompanies}
              onChange={(e) => handleExpCompanies(e, index)}
            />
            
          </Col>
        </Form.Group>
      </div>
    </>
  );
}

function ExperienceInfo({ setCvInfo, onPrev, experienceInfo, setExperienceInfo, handleFormSubmit, cvInfo, edit, handleUpdateResume, dataToUpdate,setSubmit,submit}) {
  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  // const [submit, setSubmit] = useState(false);

  const handleExpChange = (event, index) => {
    const input = event.target;
    const inputValue = input.value;
    const numericValue = inputValue.replace(/\D/g, '');
    const limitedValue = numericValue.slice(0, 3);
    const numberValue = parseInt(limitedValue, 10);
    const finalValue = numberValue > 100 ? '100' : numberValue.toString();
    input.value = '';

    setExperienceInfo((prev) => {
      const updatedExperienceInfo = [...prev];
      updatedExperienceInfo[index] = {
        ...updatedExperienceInfo[index],
        exp_years: finalValue,
    }

    return updatedExperienceInfo;
    });
    };

    const handleExpCompanies = (selectedCompanies, formIndex) => {
    setExperienceInfo((prev) => {
      const updatedExperienceInfo = [...prev];
      updatedExperienceInfo[formIndex] = {
        ...updatedExperienceInfo[formIndex],
        companies: selectedCompanies,
      };
      return updatedExperienceInfo;
    });
    };

    const handleAddExp = () => {
      setExperienceInfo((prev) => [...prev, { exp_years: null, companies: "" }]);
    };
    

    const handleRemoveExp = (formIndex) => {
      console.log("formIndex",formIndex)
      let newArr = experienceInfo.filter((exp, index) => exp !== formIndex)
      setExperienceInfo(newArr);
    };
    

    const handleExpInfo = () => {
    let validate = experiencelInfoValidation(experienceInfo);

    if (validate) {
      setCvInfo((prevInfo) => [
        {
          ...prevInfo[0],
          ExperienceInformation: experienceInfo,
        },
      ]);
      setSubmit(true);
    }
    };

    const experiencelInfoValidation = (experienceInfo) => {
      if (experienceInfo.every((exp) => exp.exp_years && exp.companies.length > 0)) {
        return true;
      } else {
        setError(true);
        setErrorMsg('Fill all fields');
        return false;
      }
    };

    useEffect(() => {
      if (submit && !edit) {
        setSubmit(false)
        handleFormSubmit();
      }
      if (submit && edit) {
        setSubmit(false)
        handleUpdateResume();
      }
    }, [submit]);

    console.log('exper',experienceInfo)
    return (
    <>
      <Card className="px-4">
        <Card.Body>
          <div className="mb-3 mt-md-4">
            <h2 className="fw-bold mb-2 text-center text-uppercase">Experience Info.</h2>
            <Row>
              <Col md={12} style={{ textAlign: 'start' }}>
                <Button onClick={handleAddExp}>Add More</Button>
              </Col>
            </Row>
            {error && <h3 className="text-center" style={{ color: 'red' }}>{errorMsg}</h3>}
            <div className="mb-3">
              <Form>
                {experienceInfo.map((formIndex,index) => (
                  <ExpForm
                    index={index}
                    key={index}
                    formIndex={formIndex}
                    handleExpChange={handleExpChange}
                    experienceInfo={experienceInfo}
                    handleExpCompanies={handleExpCompanies}
                    onRemove={() => handleRemoveExp(formIndex)}
                  />
                ))}
                <div className="d-grid mt-">
                  <Row>
                 
                    <Col md={12} style={{ textAlign: 'end' }}>
                      <Button variant="primary" type="button" onClick={handleExpInfo}>
                        {edit ? "Update" : "Finish"}
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Form>
            </div>
          </div>
        </Card.Body>
      </Card>
    </>
    )
}

export default ExperienceInfo
