// import React, { useEffect, useState } from 'react';
// import {Modal, ModalHeader, ModalTitle,Row,Col, Button} from 'react-bootstrap'
// import html2canvas from "html2canvas";
// import { jsPDF } from "jspdf";
// import html2pdf from 'html2pdf.js';


// function TemplatePreView({preViewModal,tempHtml,setPreViewModal,resumeData}) {
//     const [resumeHtml, setResumeHtml] = useState('')
//     useEffect(()=>{     
//       Object.keys(resumeData).forEach(key => {
//         const regex = new RegExp(`{{${key}}}`, "g");
//         tempHtml = tempHtml.replace(regex, resumeData[key]);
//       });
//       setResumeHtml(tempHtml)
//     },[tempHtml])

//     const downloadResume = async(resumeHtml)=>{

//       console.log("resumeHtml",resumeHtml)
//       let data = html2pdf().set({ resumeHtml }).save();
//     }
//     // let uapdatedString = tempHtml.replace(/^`|`$/g, '');
//     return (
//         <Modal
//         width={700} 
//         height={500}
//         show={preViewModal}
//         onHide={() => setPreViewModal(false)}
//         dialogClassName="modal-90w"
//         aria-labelledby="example-custom-modal-styling-title"
//       > <Modal.Header closeButton>
//         <Modal.Title id="example-custom-modal-styling-title">
//             Pre View 
//         </Modal.Title>
//         </Modal.Header>
//         <Modal.Body>  
//             <div dangerouslySetInnerHTML={{ __html:  resumeHtml }} />
//         </Modal.Body>
//         <Modal.Footer>  
//             <Button onClick={async ()=> await downloadResume(resumeHtml)}>Download Pdf</Button>
//         </Modal.Footer>
//       </Modal>
     
//       );
// }   

// export default TemplatePreView;

import React, { useEffect, useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { saveAs } from 'file-saver';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import Template1 from '../templates/Template1';
import Template2 from '../templates/Template2'
function TemplatePreView({ preViewModal, tempHtml, setPreViewModal, resumeData , setShowModal}) {
  const [resumeHtml, setResumeHtml] = useState('');
  console.log("resumeHtml",tempHtml)
 
  const downloadResume = async (resumeHtml) => {
    const canvas = await html2canvas(document.getElementById('resume-container'));
    const imgData = canvas.toDataURL('image/png');
    const pdf = new jsPDF();
    pdf.addImage(imgData, 'PNG', 0, 0, pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight());
    pdf.save('resume.pdf');
    
  };

  return (
    <Modal
      width={1200}
      height={500}
      size="lg"
      show={preViewModal}
      onHide={() => {
        setShowModal(false)
        setPreViewModal(false)
      }}
      dialogClassName="modal-90w"
      aria-labelledby="example-custom-modal-styling-title"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-custom-modal-styling-title">Pre View</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {tempHtml === 1 && 
          <div id="resume-container">
            <Template1 resumeData={resumeData}/>
          </div>
        }
        {tempHtml === 2 && 
          <div id="resume-container">
            <Template2 resumeData={resumeData}/>
          </div>
        }
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={async () => await downloadResume(resumeHtml)}>Download Pdf</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default TemplatePreView;
