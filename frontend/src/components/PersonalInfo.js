import React,{useState} from 'react';
import {Card, Form, Button,Row,Col} from 'react-bootstrap'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {Link, useAsyncError} from 'react-router-dom'
import {checkEmail, checkPhone} from '../utils/index'
import moment from 'moment'
import '../style.css'
function PersonalInfo({cvInfo,setCvInfo,onNext,personalInfo,setPersonalInfo,setCommonError}) {
  const [value, onChange] = useState(new Date());
  const [error, setError] = useState(false)  
  const [errorMsg, setErrorMsg] = useState("")
  const [startDate, setStartDate] = useState(new Date());
  
 
  
  const handleCheckGender = async(e)=>{
    setPersonalInfo(prev => {
     return {
      ...prev,
      "gender": e.target.value
     }
    })
  }

  const handleDate = async(date)=>{
    setStartDate(date)
    setPersonalInfo(prev => {
      return {
       ...prev,
       "dateOfBirth": date
      }
     })
  }

  const handleChange = async(e)=>{
    setPersonalInfo({ ...personalInfo, [e.target.name]: e.target.value })
  }

  const handlePersonalInfo = async()=>{
    let validate = await personalInfoValidation(personalInfo)
    if(validate){
      setCvInfo(prevInfo => [{
          ...prevInfo[0],
          PersonalInformation: personalInfo
        }]);
      onNext()
    }
  }

  const personalInfoValidation = async(personalInfo)=>{
    if(personalInfo?.name && personalInfo?.email && personalInfo.gender,personalInfo?.dateOfBirth && personalInfo?.phoneNumber){
      let validateEmail = await checkEmail(personalInfo?.email)
      let validatePhone = await checkPhone(personalInfo?.phoneNumber)
      if(!validateEmail){
        setError(true)
        setErrorMsg("Invalid Email")
        return false
      }else if(!validatePhone){
        setError(true)
        setErrorMsg("Invalid Phone")
        return false
      }else{
        return true
      }
    }else{
      setError(true)
      setErrorMsg("Fill all fields")
    }
  }

  const handlePhoneChange = async(event)=>{
    const input = event.target;
    const inputValue = input.value;
    console.log("inputValue",inputValue);
    // Remove any non-numeric characters
    const numericValue = inputValue.replace(/\D/g, '');
  
    // Limit the length to 10 characters
    const limitedValue = numericValue.slice(0, 10);
  
    // Set the input value to an empty string
    input.value = '';
    setPersonalInfo(prev => {
      return {
       ...prev,
       "phoneNumber": limitedValue
      }
     })
  }
  console.log("personalInfo",personalInfo)
    return (
    <>
  

      <Card className="px-4">
        <Card.Body>
          <div className="mb-3 mt-md-4">
            <h2 className="fw-bold mb-2 text-center text-uppercase ">
              Personal Info.
            </h2>
            {error && <h3 className="text-center" style={{color:"red"}}>{errorMsg}</h3>}
            <div className="mb-3">
              <Form >

              <Form.Group className="mb-3" controlId="name">
                  <Form.Label className="form-label text-center">
                      Name
                  </Form.Label>
                  <Form.Control type="text" placeholder="Enter Name"  name='name' value={personalInfo.name} onChange={handleChange}/>
              </Form.Group>
                
              <Form.Group className="mb-3" controlId="email">
                  <Form.Label className="form-label text-center">
                      Email address
                  </Form.Label>
                  <Form.Control type="email" placeholder="Enter email" name='email' value={personalInfo.email} onChange={handleChange}/>
              </Form.Group>

              
              <div className='col-md-12 mb-3'>
              <Form.Label className="form-label text-center" style={{display:'block'}}>
                Gender
              </Form.Label>
              {['Male','Female','Other'].map((ele)=>{
                return(
                  <Form.Group key ={ele} className='col-md-4' style={{display:'inline'}} > 
                    <Form.Check
                      inline
                      label={ele}
                      value={ele}
                      id='gender'
                      name='gender'
                      type="radio"
                      defaultChecked={personalInfo.gender === ele ? true : false}
                      onChange={handleCheckGender}
                    />
                  </Form.Group> 
                )

              })}
              </div> 
              <div className='col-md-12 mb-3'>
                  <Form.Label className="form-label text-center">
                    Date of Birth (MM/DD/YYYY)
                  </Form.Label>
                <DatePicker 
                  selected={personalInfo.dateOfBirth} 
                  filterDate = {(date) => {
                    return moment() > date;
                  }}
                  onChange={(date)=>handleDate(date)} 
                />

              </div>

              <div className="mb-3">
                <label className="form-label">Phone:</label>
                <div className="form-group mt-2 d-inline-block">
                    <span className="border-end country-code px-2">+91</span>
                    <input type="number" className="form-control" placeholder="Enter Phone Number" name='phoneNumber' id='phoneNumber' value={personalInfo.phoneNumber} maxlength={10} onChange={handlePhoneChange} />
                </div>
              </div>

              <div className="d-grid">

                <Row>
                  <Col md={12} style={{textAlign:'end'}}>
                  <Button variant="primary" type="button" onClick={handlePersonalInfo}>
                    Next
                  </Button>
                  </Col>
                </Row>
              </div>
              </Form>
            </div>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}   

export default PersonalInfo;

