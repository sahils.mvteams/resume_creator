import React,{useEffect, useState} from 'react';
import {Card, Button, Form, Row, Col} from 'react-bootstrap'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Combobox from "react-widgets/Combobox";
import moment from 'moment'

function EducationalInfo({setCvInfo,onNext,onPrev,graduateInfo, setGraduateInfo,postgraduateInfo, setPostGraduateInfo,listOfInstitutes}) {
  const [ error, setError] = useState("")
  const [ errorMsg, setErrorMsg] = useState("")
  const [startDate, setStartDate] = useState(new Date()); 


  const handleGraduatePassingYear = async(date)=>{
    setStartDate(date)
    setGraduateInfo(prev => {
      return {
       ...prev,
       "passing_year": date
      }
     })
  }

  const handleGraduateCollegeInfo = async(e)=>{
    setGraduateInfo(prev => {
      return {
       ...prev,
       "college_uni_name": e
      }
     })
  }

  const handlePostGraduateCollegeInfo = async(e)=>{
    setPostGraduateInfo(prev => {
      return {
       ...prev,
       "post_college_uni_name": e
      }
     })
  }

  const handlePostGraduatePassingYear = async(date)=>{
    setStartDate(date)
    setPostGraduateInfo(prev => {
      return {
       ...prev,
       "post_passing_year": date
      }
     })
  }

  const handleMarksChange = async(event)=>{

    console.log("event.target.name",event.target.value)
    const input = event.target;
    const inputValue = input.value;
    const numericValue = inputValue.replace(/\D/g, '');
  
    const limitedValue = numericValue.slice(0, 3);
  
    const numberValue = parseInt(limitedValue, 10);

    console.log("numberValue",numberValue)
    const finalValue = numberValue > 100 ? '100' : isNaN(numberValue) ? '' : numberValue.toString();
    console.log("finalValue",finalValue)
    input.value = '';
    if(event.target.name === "post_marks"){   
      setPostGraduateInfo(prev => {
        return {
         ...prev,
         "post_marks": finalValue
        }
       })
    }else{
      setGraduateInfo(prev => {
        return {
         ...prev,
         "marks": finalValue
        }
       })
    }
  }
  

  const educationalInfoValidation = async(graduateInfo,postgraduateInfo)=>{
    if(graduateInfo?.education && graduateInfo?.college_uni_name && graduateInfo?.passing_year && graduateInfo?.marks){
      if(Object.keys(postgraduateInfo).length > 0 && (postgraduateInfo?.post_college_uni_name !=='' || postgraduateInfo?.post_marks !== '')){

        if(postgraduateInfo?.education && postgraduateInfo?.post_college_uni_name && postgraduateInfo?.post_passing_year && postgraduateInfo?.post_marks){
          return true
        }else{
          setError(true)
          setErrorMsg("Fill all fields post graduation")
          return false
        }
      }
      return true
    }else{
      setError(true)
      setErrorMsg("Fill all fields")
    }
  }

  const handleEducationInfo = async()=>{
    // setCvInfo(prev=>[...prev,graduateInfo,postgraduateInfo])
    let validate = await educationalInfoValidation(graduateInfo,postgraduateInfo)
    if(validate){
      setCvInfo(prevInfo => [{
        ...prevInfo[0],
        EducationalInformation:{ graduateInfo,postgraduateInfo}
      }]);
      onNext()
    }
  }
  console.log("postgraduateInfo",postgraduateInfo)

    return (
      <>
      <Card className="px-4">
        <Card.Body>
          <div className="mb-3 mt-md-4">
           <h2 className="fw-bold mb-2 text-center text-uppercase ">
              Educational Info.
            </h2>
            {error && <h3 className="text-center" style={{color:"red"}}>{errorMsg}</h3>}
            <Form >
            <h3 style={{textAlign:'left'}}>Graduation</h3>
            <div className="mb-3 px-4 py-4" style={{border:'1px solid grey', borderRadius:'5px'}}>
              <Form.Group
                className="mb-3"
                controlId="college_uni_name"
              >
              <Form.Label className='form-label text-center'> University/College </Form.Label>
              <Combobox
                // value={graduateInfo.college_uni_name}
                data={listOfInstitutes}
                dataKey='id'
                textField='name'
                defaultValue={graduateInfo.college_uni_name}
                onChange={handleGraduateCollegeInfo}
              />
              </Form.Group>

              <div className='col-md-12 mb-3'>
              <Form.Label className='form-label text-center'> Passing year</Form.Label>
                <DatePicker selected={startDate} onChange={(date)=>{handleGraduatePassingYear(date)}} filterDate = {(date) => {
                    return moment() > date;
                  }}/>
              </div>
              
              <Form.Group className="mb-3" controlId="marks">
                  <Form.Label className="form-label text-center">
                      Marks(percentage%)
                  </Form.Label>
                  <Form.Control type="number" placeholder="Enter Marks" onChange={handleMarksChange} name='marks'  value={graduateInfo.marks} maxLength="2"/>
              </Form.Group>
            </div>

            <h3 style={{textAlign:'left'}}>Post Graduation(*Optional)</h3>
            <div className="mb-3 px-4 py-4" style={{border:'1px solid grey', borderRadius:'5px'}}>
              <Form.Group
                className="mb-3"
                controlId='post_college_uni_name'
              >
              <Form.Label className='form-label text-center'> University/College</Form.Label>
              <Combobox
                // value={graduateInfo.college_uni_name}
                data={listOfInstitutes}
                dataKey='id'
                textField='name'
                defaultValue={postgraduateInfo.post_college_uni_name}
                onChange={handlePostGraduateCollegeInfo}
              />
              </Form.Group>

              <div className='col-md-12 mb-3'>
              <Form.Label className='form-label text-center'> Passing year</Form.Label>
                <DatePicker selected={startDate} onChange={(date) => handlePostGraduatePassingYear(date)} filterDate = {(date) => {
                    return moment() > date;
                  }}/>
              </div>
              
              <Form.Group className="mb-3" controlId="marks">
                  <Form.Label className="form-label text-center">
                      Marks(percentage%)
                  </Form.Label>
                  <Form.Control type="number" placeholder="Enter Marks" onChange={handleMarksChange} name='post_marks' value={postgraduateInfo.post_marks} maxLength={3}/>
              </Form.Group>
            </div>
            <div className="d-grid">
              <Row>
                <Col md={6} style={{textAlign:'start'}}>
                    <Button  onClick={()=>onPrev()}>Prev</Button>
                </Col>
                <Col md={6} style={{textAlign:'end'}}>
                <Button variant="primary" type="button" onClick={handleEducationInfo}>
                  Next
                </Button>
                </Col>
              </Row>
            </div>
            </Form>

            
          </div>
        </Card.Body>
      </Card>
    </>
      );
}   

export default EducationalInfo;