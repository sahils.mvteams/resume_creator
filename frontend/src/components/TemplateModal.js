import React, { useState } from 'react';
import {Modal, ModalHeader, ModalTitle,Row,Col} from 'react-bootstrap'
import {Link, useAsyncError} from 'react-router-dom'
import Templates from './Templates'

function TemplateModal({showModal,setShowModal, resumeData}) {

    return (
        <Modal
        width="auto"
        show={showModal}
        onHide={() => setShowModal(false)}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Choose Templates
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>  
          <Templates resumeData={resumeData} setShowModal={setShowModal}/>
        </Modal.Body>
      </Modal>
     
      );
}   

export default TemplateModal;