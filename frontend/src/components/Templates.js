import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {Row,Col, Container} from 'react-bootstrap'
import {Link, useAsyncError} from 'react-router-dom'
import TemplatePreView from './TemplatePreView'
import Template1 from "../templates/Template1"
function Templates({resumeData,setShowModal}) {
    const [allTemplates, setAllTemplates] = useState([])
    const [preViewModal, setPreViewModal] = useState(false)
    const [tempHtml, setTempHtml] =  useState('');
    useEffect(()=>{
        (async()=>{
            let response = await axios.get("http://localhost:4000/api/resume/fetchTemplates",{headers: {"x-auth-token": window.localStorage.getItem('token')}})
            if(response?.data?.success){
                setAllTemplates(response?.data?.allTemplates)
            }
        })()
    },[])

    const handleClickTemplate = async(temp_html)=>{
        setTempHtml(temp_html)
        setPreViewModal(true)
    }
    
return (
    <>
        <>
            <Row>
                {allTemplates && allTemplates.length > 0 &&

                    allTemplates.map((e)=>{
                        return (
                            <>
                                <Col md={5} onClick={()=>handleClickTemplate(1)} style={{cursor:'pointer'}} height >
                                    <div>
                                        <img src={`http://localhost:3000/assets/${e.image_path}`} width="100%" style={{border:'1px solid black'}}></img>
                                    </div>     
                                </Col>
                                <Col md={5} onClick={()=>handleClickTemplate(2)} style={{cursor:'pointer'}} height >
                                <div>
                                    <img src={`http://localhost:3000/assets/${e.image_path}`} width="100%" style={{border:'1px solid black'}}></img>
                                </div>     
                                </Col>
                            </>
                        )
                    })   
                }        
            </Row>
            <TemplatePreView preViewModal={preViewModal} tempHtml={tempHtml} setPreViewModal={setPreViewModal} resumeData={resumeData} setShowModal={setShowModal}/>
        </>
    </>
    );
}   

export default Templates;