  import React, { useEffect, useState } from 'react';
  import {Card, Button, Row, Col, Container,Table} from 'react-bootstrap'
  import axios from 'axios'
  import TemplateModal from './TemplateModal';
  import { openNotification } from '../utils';
import { resolvePath } from 'react-router-dom';
  function ResumeList({setShow,setEdit,setDataToUpdate}) {
    const [allCvInfo, setAllCvInfo] = useState([])
    const [showModal,setShowModal] = useState(false)
    const [fetchResume, setFetchResume] = useState(false)
    const [resumeData, setResumeData] = useState({})
    useEffect(()=>{
      (async()=>{
        let response = await axios.get('http://localhost:4000/api/resume/fetchAll',{headers: {"x-auth-token": window.localStorage.getItem('token')}})
        console.log("response",typeof response.status)
        if(response.status === 201){
          setAllCvInfo(response.data.allResumeData)
        }
      })()
    },[fetchResume])


    const handleDeleteResume = async(id)=>{
      let response = await axios.delete(`http://localhost:4000/api/resume/delete/${id}`,{headers: {"x-auth-token": window.localStorage.getItem('token')}})
      if(response?.data?.success){
        await openNotification('Success','Deleted Successfully')
        setFetchResume(true)
      }
    }
    const handleTemplate = async (event,ele)=> {
      console.log("ele:::",ele)
      let values = await getAllResumeValues(ele)
      console.log("values:::::::::::;;",values)      
      setResumeData(values)
      setShowModal(true);
    }

    const getAllResumeValues = async(ele)=>{
      let values  = {
        resume_id: ele.id,
        name: ele.personal_info.name,
        email: ele.personal_info.email,
        gender: ele.personal_info.gender,
        dob: ele.personal_info.dateOfBirth,
        phone: ele.personal_info.phoneNumber,

      }
      if(ele.educational_info){
        for (var k in ele.educational_info){
          let educational_info = ele.educational_info[k]
          if(educational_info.education === 'graduation'){
            values['graduate_uni']= educational_info.college_uni_name
            values['graduate_pass_year']= educational_info.passing_year 
            values['graduatemarks']= educational_info.marks
          }
          if(educational_info.education === 'postgraduation'){
            values ['post_graduate_uni']= educational_info.college_uni_name
            values ['post_graduate_pass_year']= educational_info.passing_year
            values ['post_graduatemarks']= educational_info.marks
          }
        }
        
      }
      let arr = []
      for (let k in ele.experience_info){
        let expInfo = ele.experience_info[k]
        // delete expInfo['id']
        arr.push(expInfo)
      }
      values['expInfo'] = arr
      return values
    }

    const handleEditResume = async(ele)=>{
      let values = await getAllResumeValues(ele)
      console.log("values",values)
      setDataToUpdate(values)
      setEdit(true)
      setShow('updatecv')
    }
      return (
        <>
          <TemplateModal showModal={showModal} setShowModal={setShowModal} resumeData={resumeData}/>
          <Container fluid>
              <Row className="vh-300 d-flex justify-content-center align-items-center">
                <Col md={12} lg={12} xs={12}>
              
                <Table striped bordered hover variant="dark">
                <thead>
                  <tr>
                    <th>#Resume</th>
                    <th>Personal Info</th>
                    <th>Educational Info</th>
                    <th>Experience Info</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                <tbody>
                {allCvInfo.length > 0 
                ?
                allCvInfo.map((ele,idx)=>{
                return(
                  <tr>
                    <td>
                      {idx+1}
                    </td>

                    {/*For personal info  */}

                    <td>
                      <tr>
                        <td>Name:</td>
                        <td>{ele.personal_info.name}</td>
                      </tr>
                      <tr>
                        <td>Email:</td>
                        <td>{ele.personal_info.email}</td>
                      </tr>
                      <tr>
                        <td>Gender:</td>
                        <td>{ele.personal_info.gender}</td>
                      </tr>
                      <tr>
                        <td>D.O.B:</td>
                        <td>{new Date(ele.personal_info.dateOfBirth).toLocaleDateString()}</td>
                      </tr>
                      <tr>
                        <td>Phone:</td>
                        <td>{ele.personal_info.phoneNumber}</td>
                      </tr>
                    </td>

                    {/*For Education  */}

                    <td> 
                      <h4>Graduation</h4>
                        <tr>
                          <td>Education:</td>
                          <td>{ele.educational_info[0].education}</td>
                        </tr>
                        <tr>
                          <td>College/Uni:</td>
                          <td>{ele.educational_info[0].college_uni_name}</td>
                        </tr>
                        <tr>
                          <td>Passing Year:</td>
                          <td>{new Date(ele.educational_info[0].passing_year).toLocaleDateString()}</td>
                        </tr>
                        <tr>
                          <td>Marks:</td>
                          <td>{ele.educational_info[0].marks}%</td>
                        </tr>

                      {ele.educational_info.length > 1 &&
                      
                        <>
                        <hr/>
                        <h4>Post Graduation</h4>
                          <tr>
                            <td>Education:</td>
                            <td>{ele.educational_info[1].education}</td>
                          </tr>
                          <tr>
                            <td>College/Uni:</td>
                            <td>{ele.educational_info[1].college_uni_name}</td>
                          </tr>
                          <tr>
                            <td>Passing Year:</td>
                            <td>{new Date(ele.educational_info[1].passing_year).toLocaleDateString()}</td>
                          </tr>
                          <tr>
                            <td>Marks:</td>
                            <td>{ele.educational_info[1].marks}%</td>
                          </tr>
                        </>
                        }
                  
                    </td>

                    {/*For Experience  */}
                    
                    <td>
                    {ele?.experience_info && ele.experience_info. length >0 &&
                    ele.experience_info.map((ele)=>{
                      return (
                      <>
                      {/* {console.log('ele',ele)} */}
                        <tr>
                          <td>Years of Exp.:</td>
                          <td>{ele.exp_years}</td>
                        </tr>
                        <tr>
                          <td>Companies:</td>
                          {ele.companies}
                        </tr>
                        <hr/>
                      </>
                      )
                    })
                    }
                    </td>

                    {/*For Actions  */}
                    
                    <td>
                      <tr>
                        <td>
                          <Button className='btn btn-primary mx-3' onClick={()=>{
                            handleEditResume(ele)
                          }}>Edit</Button>
                        </td>
                        <td>
                          <Button className='btn btn-danger' onClick={()=>{
                            handleDeleteResume(ele.id)
                          }}>
                            Delete
                          </Button>
                        </td>
                        <td>
                          <Button className='btn btn-info mx-3' 
                          onClick={(event)=>handleTemplate(event,ele)}
                          >
                            View
                          </Button>
                        </td>
                      </tr>
                    </td>
                  </tr>
                    )
                  })
                  :
                  <h3>No Resume</h3>
                }
                </tbody>
              </Table>
            </Col>
            
          </Row>
          
        </Container>
        </>     
    );
  }   

  export default ResumeList;