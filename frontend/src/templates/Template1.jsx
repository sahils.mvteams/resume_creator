import React from 'react';
import "./temp1.css"
const Template1 = ({ resumeData }) => {

    return (
        <div className="resume-container">
            <h1 style={{textAlign:'center'}}><u>{resumeData.name}</u></h1>
            <div className="section">
                <h3>Personal Information</h3>
                <ul>
                    <li> <p>Email: {resumeData.email}</p></li>
                    <li><p>Gender: {resumeData.gender}</p></li>
                    <li><p>Date of Birth: {new Date(resumeData.dob).toLocaleDateString()}</p></li>
                    <li><p>Phone: {resumeData.phone}</p></li>
                </ul>
               
                               
                </div>

            <div className="section">
                <h3>Educational Information</h3>
               {resumeData.graduate_uni &&
                <div>
                        <h4 style={{marginLeft:'10px'}}>Graduation</h4>
                        <ul style={{marginLeft:'10px'}}>
                            <li><p>University/College: {resumeData.graduate_uni}</p></li>
                            <li><p>Passing Year: {new Date(resumeData.graduate_pass_year).toLocaleDateString()}</p></li>
                            <li><p>Marks: {resumeData.graduatemarks}%</p></li>
                        </ul>
                </div>
                }
                {resumeData.post_graduate_uni &&
                    <div>
                        <h4 style={{marginLeft:'10px'}}>Post Graduation</h4>
                        <ul style={{marginLeft:'10px'}}>
                            <li><p>University/College: {resumeData.post_graduate_uni}</p></li>
                            <li><p>Passing Year: {new Date(resumeData.post_graduate_pass_year).toLocaleDateString()}</p></li>
                            <li><p>Marks: {resumeData.post_graduatemarks}%</p></li>  
                        </ul>
                        
                    </div>
                }
            </div>

            <div className="section">
                <h3>Experience Information</h3>
                {resumeData.expInfo.map((experience, index) => (
                    <div key={index} className="experience-item">
                        <h4 style={{marginLeft:'10px'}}>Experience {index + 1}</h4>
                        <ul style={{marginLeft:'10px'}}>
                            <li><p>Year of Experience: {experience.exp_years}</p></li>
                            <li><p>Company: {experience.companies}</p></li>
                        </ul>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Template1;
