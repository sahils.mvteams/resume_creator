import React,{useState} from 'react'
import {useNavigate,Link} from 'react-router-dom'
import "../style.css" 
import axios from 'axios'
import {Container, Row, Col, Card, Form, Button} from 'react-bootstrap'
import {checkEmail,openNotification} from "../utils/index"

function LogIn(){
  const navigate = useNavigate()
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [error, setError] = useState(false)
  const [errorMsg, setErrorMsg] = useState("")

  const handleEmail = (e)=>{
    setError("")
    setErrorMsg("")
    setEmail(e.target.value)  
  }

  const handlePassword = (e)=>{
    setError("")
    setErrorMsg("")
    setPassword(e.target.value)
  }

	const handleLogin = async(e)=>{
    e.preventDefault()
        try{
          let validateMail = await checkEmail(email)
          if(validateMail){
            setError(false)
            setErrorMsg("") 
            let login =  await axios.post('http://localhost:4000/api/user/login',{email,password})
            if(login.data.success){
              localStorage.setItem("token",login.data.token)
              localStorage.setItem("isAuthenticated",true)
              await openNotification('Success','LogIn Successfully')
              navigate('/')
            }else{
              setError(true)
              setErrorMsg(login.response.data.msg)
            }
          }else{
            setError(false)
            setErrorMsg("Invalid Email")
          }
        }catch(err){
          setError(true)
          setErrorMsg(err.response.data.msg)
        }
    }

  
	return(
            <div>
              <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                  <Col md={8} lg={6} xs={12}>
                    <Card className="px-4">
                      <Card.Body>
                        <div className="mb-3 mt-md-4">
                          <h2 className="fw-bold mb-2 text-center text-uppercase ">
                            LogIn Here
                          </h2>
                          {error && <h3 className="text-center" style={{color:"red"}}>{errorMsg}</h3>}
                          <div className="mb-3">
                            <Form onSubmit={handleLogin}>
                              <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label className="text-center">
                                  Email address
                                </Form.Label>
                                <Form.Control type="email" placeholder="Enter email" onChange={handleEmail}/>
                              </Form.Group>
        
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={handlePassword}/>
                              </Form.Group>

                              <Form.Group
                                className="mb-3"
                                controlId="formBasicCheckbox"
                              ></Form.Group>

                              <div className="d-grid">
                                <Button variant="primary" type="submit">
                                  Sign In
                                </Button>
                              </div>
                            </Form>
                            <div className="mt-3">
                              <p className="mb-0  text-center">
                                Don't have an account??{' '}
                                <Link to="/register">
                                  Register Here
                                </Link>
                              </p>
                            </div>
                          </div>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>
            </div>
          );
}
export default LogIn;