import React,{useState} from 'react'
import {useNavigate,Link} from 'react-router-dom'
import "../style.css" 
import {checkEmail} from "../utils/index"
import {Container, Row, Col, Card, Form, Button} from 'react-bootstrap'
import axios from 'axios'
import { openNotification } from '../utils/index'
function Register(){
  const navigate = useNavigate()
  const [fullName, setFullName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [error, setError] = useState(false)
  const [errorMsg, setErrorMsg] = useState("")

  const handleName = (e)=>{
    setError("")
    setErrorMsg("")
    setFullName(e.target.value)
  }
  const handleEmail = (e)=>{
    setError("")
    setErrorMsg("")
    setEmail(e.target.value)  
  }

  const handlePassword = (e)=>{
    setError("")
    setErrorMsg("")
    setPassword(e.target.value)
  }

  const handleRegister = async(e)=>{
    e.preventDefault()
    try{
        let validation = await handleValidation(fullName, email, password)
        if(validation){
          setError(false)
          setErrorMsg("")

          let signup =  await axios.post('http://localhost:4000/api/user/signup',{fullName,email,password})
            if(signup.data.success){
              await openNotification('Success','Registered Successfully')
              navigate('/login')
            }else{
              setError(true)
              setErrorMsg(signup.response.data.msg)
            }
        }
    }catch(err){
      setError(true)
      setErrorMsg(err.response.data.msg)
    }

}

const handleValidation = async(fullName, email, password)=>{
    if(!fullName || !email || !password){
      setError(true)
      setErrorMsg("Fill all fields")
      return false
    }
    let validatEmail = await checkEmail(email)
    if(!validatEmail){
      setError(true)
      setErrorMsg("Enter valid mail")
      return false
    }
    return true
  }

	
	return(
            <div>
              <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                  <Col md={8} lg={6} xs={12}>
                    <Card className="px-4">
                      <Card.Body>
                        <div className="mb-3 mt-md-4">
                          <h2 className="fw-bold mb-2 text-center text-uppercase ">
                            Register Here
                          </h2>
                          {error && <h3 className="text-center" style={{color:"red"}}>{errorMsg}</h3>}
                          <div className="mb-3">
                            <Form onSubmit={handleRegister}>

                            <Form.Group className="mb-3" controlId="formBasicName">
                                <Form.Label className="text-center">
                                    Full Name
                                </Form.Label>
                                <Form.Control type="text" placeholder="Enter Name" onChange={handleName}/>
                            </Form.Group>
                              
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label className="text-center">
                                    Email address
                                </Form.Label>
                                <Form.Control type="email" placeholder="Enter email" onChange={handleEmail}/>
                            </Form.Group>
    
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={handlePassword}/>
                            </Form.Group>

                              <div className="d-grid">
                                <Button variant="primary" type="submit">
                                  Register
                                </Button>
                              </div>
                            </Form>
                            <div className="mt-3">
                              <p className="mb-0  text-center">
                               Already have an account??{' '}
                                <Link to="/login">
                                  LogIn Here
                                </Link>
                              </p>
                            </div>
                          </div>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Container>
            </div>
          );
}
export default Register;