import React,{useEffect, useState}from 'react';
import GenerateResume from '../components/GenerateResume';
import ResumeList from '../components/ResumeList';
import { Button, Col, Container, Row } from 'react-bootstrap';
import CreateResume from '../components/CreateResume'
function Dashboard() {
    const [show ,setShow] = useState("cvlist")
    const [edit, setEdit] = useState(false)
    const [dataToUpdate, setDataToUpdate] = useState()
    const handleCreateCv = async()=>{
        setShow("createcv")
    }
    return (  
        <Container fluid>
            <Row>
                <Col md={12} style={{backgroundColor:'black',}} className='mt-5'>
                    <Row>
                    {
                        show === "cvlist" && 
                        <>
                            <Col md={8}>
                                <h1 style={{color:'white'}} className='mt-2'>CV List</h1>
                            </Col>
                            <Col md={4} style={{textAlign:'end'}}>
                            
                                <Button  size="lg" className='mt-2 mb-2' onClick={handleCreateCv}>
                                    Build Your CV
                                </Button>
                        
                            </Col>
                        </>
                    }  
                    </Row>
                </Col>
                <Col md={12} className='mt-5'>
                    {
                        (show === "createcv" || show === "updatecv") ? <CreateResume edit={edit} dataToUpdate={dataToUpdate}/>   : <ResumeList setDataToUpdate={setDataToUpdate} setEdit={setEdit} setShow={setShow}/>
                    }   
                </Col>
               
            </Row>

            
        </Container>
    );
}

export default Dashboard;