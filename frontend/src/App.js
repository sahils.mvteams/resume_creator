import React from 'react'
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard.js'
import {BrowserRouter,Routes,Route,useNavigate} from 'react-router-dom'
import PrivateRoute from './PrivateRoute';
import ProtectedRoute from  './ProtectedRoute'
import 'react-notifications/lib/notifications.css';
import { NotificationContainer } from 'react-notifications';
import "./style.css"
import Header from './components/Header'
import {Row,Col,Container} from 'react-bootstrap'
import "react-widgets/styles.css";

function App() {
  return (
    <>
      <NotificationContainer />
      <BrowserRouter>
      <Container fluid style={{backgroundColor:"black"}}>
            <Row>
                <Col md={12} className=' nav-bar' >
                    <Header/>
                </Col>

            </Row>

        </Container>
        <Routes>    
          <Route exact path='/' 
            element={
              <PrivateRoute>
                <Dashboard/>
              </PrivateRoute>
          }/> 

          <Route exact path='/register' 
            element={
              <ProtectedRoute>
                <Register/>
              </ProtectedRoute>
          }/> 
          <Route exact path='/login' 
          element={
            <ProtectedRoute>
              <Login/>
            </ProtectedRoute>
          }/>    
                            
        </Routes>
      </BrowserRouter>
    </>

  );
}

export default App;