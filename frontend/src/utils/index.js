import { NotificationManager } from 'react-notifications';

export const checkEmail = async(email)=>{
    var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!filter.test(email)) {
      return false;
    }else{
      return true;
    }
  }

  
export const checkPhone = async(phoneNumber)=>{
    if(phoneNumber.length !== 10){
      return false
    }
    return true
}

export const openNotification = async(type,msg)=>{
  if(type === 'Success'){
    NotificationManager.success(msg ,type,2000)
  }
}


