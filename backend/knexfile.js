module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      user: 'root',
      password: 'testpassword',
      charset: 'utf8',
      database: 'resume_builder',
    },
    migrations: {
      directory:__dirname + '/migrations' // Update this line with the correct path to your migrations directory
    }
  },
};
