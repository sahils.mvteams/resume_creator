const { bookshelfInstance } = require('../index.js'); // Import the bookshelfInstance from your Knex setup file
const PersonalInfo = require('../models/personalInfo.model.js')
const EducationalInfo = require('../models/educationalInfo.model.js')
const ExperienceInfo = require('../models/experienceInfo.model.js')
const User = require('../models/user.model.js')
const Resume = bookshelfInstance.model('Resume', {
    tableName: 'resumes',
    users() {
      return this.belongsTo(User, 'user_id');
    },
    personal_info() {
      return this.hasOne(PersonalInfo, 'resume_id');
    },
    educational_info(){
      return this.hasMany(EducationalInfo, 'resume_id')
    },
    experience_info(){
      return this.hasMany(ExperienceInfo, 'resume_id')
    }
  });
module.exports =  Resume;
