const { bookshelfInstance } = require('../index.js'); // Import the bookshelfInstance from your Knex setup file
const Resume = require('../models/resume.model.js')
const PersonalInfo = bookshelfInstance.model('PersonalInfo', {
    tableName: 'personal_info',
    resume() {
      return this.belongsTo(Resume, 'resume_id');
    }
  });
module.exports =  PersonalInfo;
