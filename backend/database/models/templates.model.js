
const { bookshelfInstance } = require('../index.js'); // Import the bookshelfInstance from your Knex setup file

const ResumeTemplates = bookshelfInstance.model('ResumeTemplates', {
    tableName: 'resume_templates',
   
  });
module.exports =  ResumeTemplates;
