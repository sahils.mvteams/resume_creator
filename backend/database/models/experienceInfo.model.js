const { bookshelfInstance } = require('../index.js'); // Import the bookshelfInstance from your Knex setup file
const Resume = require('../models/resume.model.js')
const ExperienceInfo = bookshelfInstance.model('ExperienceInfo', {
    tableName: 'experience_info',
    resume() {
        return this.belongsTo(Resume, 'resume_id');
    }
  });
module.exports =  ExperienceInfo;
