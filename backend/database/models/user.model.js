const { bookshelfInstance } = require('../index.js'); // Import the bookshelfInstance from your Knex setup file
const Resume = require('../models/resume.model.js')
const User = bookshelfInstance.model('User', {
  tableName: 'users',
  resumes() {
    return this.hasMany(Resume, 'user_id');
  }
});
module.exports =  User;
