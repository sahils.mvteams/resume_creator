const knex = require ('knex');
const bookshelf = require ('bookshelf');
const pkg = require ('../knexfile.js');
const {development} = pkg;

// const dbConfig = {
//   client: 'mysql',
//   connection: {
//     host: '127.0.0.1',
//     user: 'root',
//     password: 'testpassword',
//     charset: 'utf8',
//     db: null,
//   },
// };

const dbConfig = development
let knexInstance = knex(dbConfig);
let bookshelfInstance = bookshelf(knexInstance);
// bookshelfInstance.plugin('registry')
// Create the database if it doesn't exist
// knex(dbConfig)
//   .raw('CREATE DATABASE IF NOT EXISTS resume_builder')
//   .then(() => {
//     console.log('Database created or already exists.');

//     // Update the configuration with the database name
//     dbConfig.connection.database = 'resume_builder';
//     knexInstance = knex(dbConfig);

//     // Use the connection with the database
//     knexInstance
//       .raw('SELECT 1')
//       .then(() => {  // "type": "module",

//         console.log('Connected to the database.');
//         // Rest of your code...

//         // Assign the updated instances
//         bookshelfInstance = bookshelf(knexInstance);
//       })
//       .catch((error) => {
//         console.error('Error connecting to the database:', error);
//       });
//   })
//   .catch((error) => {
//     console.error('Error creating the database:', error);
//   });

module.exports =  { knexInstance, bookshelfInstance };
