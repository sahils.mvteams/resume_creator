const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('../database/models/user.model')
const Colleges = require('../database/models/colleges.model')
const { bookshelfInstance } = require('../database/index'); // Import the bookshelfInstance from your Knex setup file
async function registerUser(req,res){
    try{
        console.log("register")
        let {fullName, email, password} = req.body;
        console.log(req.body)
        const existingUser = await User.where({email:email}).fetch().then((user)=>{
            console.log("user",user.toJSON())
            return user.toJSON()
        }).catch(err=>{
            console.log(err)
            return null
        })
        console.log("existingUser",existingUser)
        if (existingUser)return res.status(400).json({ msg: "An account with this email already exists." });
        
        const salt = await bcrypt.genSalt();
        const passwordHash = await bcrypt.hash(password, salt);

        User.forge({fullName:fullName,email:email,password:passwordHash}).save().then((model) => {
            console.log("model",model)
            return res.json({"success":true})
        }).catch(err=>{
            console.log("err in register user",err)
            return res.json({"success":true,
            "error":err})
        })
    
        console.log('Here in "register" user')
    }catch(err){
        console.log("Error in registerUser",err)
    }
} 

async function loginUser(req,res){
    try{
        const { email, password } = req.body;
        console.log(req.body)
        const user = await User.where({email:email}).fetch().then((user)=>{
            console.log("user",user.toJSON())
            return user.toJSON()
        }).catch(err=>{
            console.log(err)
            return null
        })
        console.log("user",user)
        if (!user)return res.status(400).json({ msg: "No account with this email has been registered." });
        const isMatch = await bcrypt.compare(password, user.password);
    
        if (!isMatch) return res.status(400).json({ msg: "Invalid credentials." });
        console.log(user.id)
        const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET);
        res.json({
            "success":true,
            "token":token,
            "user": user
        });
        console.log('Here in login user')
    }catch(err){
        console.log("Error in loginUser",err)
    }
} 


async function fetchUser(req, res){
    try{
        const user = await User.findById(req.user);
        res.json({
            "user":user,
            "token":req.header("x-auth-token")
        });
    }catch(err){
        console.log("Error in fetch user",err)
    }
   
}

async function fetchColleges(req,res){
    try{
        const colleges = await Colleges.query(function (qb) {
            qb.limit(30);
          }).fetchAll()

        console.log("colleges",colleges.toJSON())
          return res.status(201).json({"colleges":colleges.toJSON()})
    }catch(err){
        console.log("Error in fetchColleges")
    }
}
module.exports = {registerUser,fetchUser,loginUser,fetchColleges}