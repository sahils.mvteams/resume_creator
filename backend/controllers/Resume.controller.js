const PersonalInfo = require('../database/models/personalInfo.model')
const EducationalInfo = require('../database/models/educationalInfo.model')
const ExperienceInfo = require('../database/models/experienceInfo.model')
const Resume = require('../database/models/resume.model')
const User = require('../database/models/user.model')
const ResumeTemplates = require('../database/models/templates.model')
const {bookshelfInstance, knexInstance} = require('../database/index')


// async function createResume(req,res){
//     bookshelfInstance.transaction(async (trx) => {
//         try{
//             const {PersonalInformation,EducationalInformation,ExperienceInformation} = req.body.data
//             console.log("req:>>>",req.body.data)
//             const userId = req.user
//             console.log(userId)
//             let data = await Resume.forge({user_id:userId}).save(null, { transacting: trx })
          
//             if(Object.keys(PersonalInformation).length > 0){
//                 PersonalInformation.resume_id = data.attributes.id
//                 console.log("PersonalInformation",PersonalInformation)
//                 PersonalInformation.dateOfBirth = new Date(PersonalInformation.dateOfBirth)
//                 console.log("date",typeof new Date(PersonalInformation.dateOfBirth))
        
//                 let personaldata = await PersonalInfo.forge(PersonalInformation).save(null, { transacting: trx })
//                 console.log("personaldata",personaldata)
//             }
//             if(Object.keys(EducationalInformation).length > 0){
//                 if(EducationalInformation?.graduateInfo){   
//                     let graduateData =  EducationalInformation.graduateInfo
//                     console.log("EducationlInformation before",graduateData)
//                     console.log("data.attributes.id:::::;;;;",data.attributes.id);
//                     graduateData.resume_id = data.attributes.id
//                     graduateData.passing_year = new Date(graduateData.passing_year)
//                     console.log("EducationlInformation after",graduateData)
//                     let educationaldata = await EducationalInfo.forge(graduateData).save(null, { transacting: trx })
//                 }
//                 if(EducationalInformation?.postgraduateInfo && EducationalInformation?.postgraduateInfo?.post_college_uni_name !== ''){
//                     let postgradData = EducationalInformation.postgraduateInfo
//                     let newdata = {
//                         education: postgradData.education,
//                         college_uni_name: postgradData.post_college_uni_name,
//                         marks: postgradData.post_marks,
//                         passing_year: new Date(postgradData.post_passing_year),
//                         resume_id:data.attributes.id
//                     }
//                     await EducationalInfo.forge(newdata).save(null, { transacting: trx })
//                 }
//             }
            
//             if(ExperienceInformation){
//                 for (let k in ExperienceInformation){
//                     let expInfo = ExperienceInformation[k]
//                     // ExperienceInformation.companies = JSON.stringify( ExperienceInformation.companies)
                    
//                     expInfo.resume_id = data.attributes.id
//                     // console.log("ExperienceInformation",ExperienceInformation)
//                     console.log("expInfo",expInfo)  

//                     await ExperienceInfo.forge(expInfo).save(null, { transacting: trx })
//                 }
                
//             }
//             return res.json({"success":true,"msg":"Resume created SuccessFully"})
//         }catch(err){
//             console.log("Error in createResume",err)
//             return res.json({"success":false,
//                 "error":err})
//         }
//     }).catch((err)=>{
//         console.log("Transaction error while creating resume",err)
//     })
// } 
async function createResume(req, res) {
    try {
      await bookshelfInstance.transaction(async (trx) => {
        const { PersonalInformation, EducationalInformation, ExperienceInformation } = req.body.data;
        const userId = req.user;
  
        const data = await Resume.forge({ user_id: userId }).save(null, { transacting: trx });
  
        if (Object.keys(PersonalInformation).length > 0) {
          PersonalInformation.resume_id = data.attributes.id;
          PersonalInformation.dateOfBirth = new Date(PersonalInformation.dateOfBirth);
  
          await PersonalInfo.forge(PersonalInformation).save(null, { transacting: trx });
        }
  
        if (Object.keys(EducationalInformation).length > 0) {
          if (EducationalInformation?.graduateInfo) {
            const graduateData = EducationalInformation.graduateInfo;
            graduateData.resume_id = data.attributes.id;
            graduateData.passing_year = new Date(graduateData.passing_year);
  
            await EducationalInfo.forge(graduateData).save(null, { transacting: trx });
          }
  
          if (
            EducationalInformation?.postgraduateInfo &&
            EducationalInformation?.postgraduateInfo?.post_college_uni_name !== ''
          ) {
            const postgradData = EducationalInformation.postgraduateInfo;
            const newdata = {
              education: postgradData.education,
              college_uni_name: postgradData.post_college_uni_name,
              marks: postgradData.post_marks,
              passing_year: new Date(postgradData.post_passing_year),
              resume_id: data.attributes.id
            };
            await EducationalInfo.forge(newdata).save(null, { transacting: trx });
          }
        }
  
        if (ExperienceInformation) {
          for (const k in ExperienceInformation) {
            const expInfo = ExperienceInformation[k];
            expInfo.resume_id = data.attributes.id;
  
            await ExperienceInfo.forge(expInfo).save(null, { transacting: trx });
          }
        }
  
        res.json({ "success": true, "msg": "Resume created successfully" });
      });
    } catch (err) {
      console.log("Error in createResume", err);
      res.json({ "success": false, "error": err });
    }
  }
  
async function fetchAllResume(req,res){
    try{
        let allResumeData = []
        let user_resumes = await User.where({id: req.user}).fetch({withRelated: ['resumes']})
        let allResumes = user_resumes.toJSON()
        let listOfResumes = allResumes.resumes
        for (var k in listOfResumes){
            let resume = listOfResumes[k]
            let data = await Resume.where({id: resume.id}).fetch({withRelated: ['personal_info','experience_info','educational_info']})
            let obj = data.toJSON()
            // console.log('obj::::>>',obj);
            allResumeData.push(obj)
        }
            return res.status(201).json({"success":true,"allResumeData":allResumeData})
    }catch(err){
        console.log("Error in loginUser",err)
    }
} 

async function deleteResume(req,res){
    try{
        console.log('Here in login user',req.params)
 
        const ResumeIdToDelete = req.params.id;

        knexInstance.transaction(async (trx) => {
          // Delete educational info first
          await trx('educational_info').where({ resume_id: ResumeIdToDelete }).del();
        
          // Delete experience info
          await trx('experience_info').where({ resume_id: ResumeIdToDelete }).del();
        
          // Delete personal info
          await trx('personal_info').where({ resume_id: ResumeIdToDelete }).del();
        
          // Delete the resume
          await trx('resumes').where({ id: ResumeIdToDelete }).del();
        })
          .then(() => {
            console.log('Resume and associated information deleted successfully');
            res.json({"success":true, "msg":"deleted successfully"})
          })
          .catch((error) => {
            console.error(error);
            res.json({"success":false})
          });
    }catch(err){
        console.log("Error in deleteResume",err)
    }
} 

async function fetchAllTemplates(req,res){
    try{
        let allTemplates = await ResumeTemplates.fetchAll()
        res.json({"success":true,"allTemplates":allTemplates.toJSON()})
    }catch(err){
        console.log("Error in fetchAllTemplates")
    }
}

async function updateResume(req,res){
    const resumeId = req.params.id
    knexInstance.transaction(async (trx) => {
        try{
            const {PersonalInformation,EducationalInformation,ExperienceInformation} = req.body.data
            const userId = req.user
          
            if(Object.keys(PersonalInformation).length > 0){
                PersonalInformation.resume_id = resumeId
                PersonalInformation.dateOfBirth = new Date(PersonalInformation.dateOfBirth)
                await trx('personal_info').where({resume_id : resumeId}).update(PersonalInformation)
            }
            if(Object.keys(EducationalInformation).length > 0){
                if(EducationalInformation?.graduateInfo){   
                    let graduateData =  EducationalInformation.graduateInfo
         
                    graduateData.resume_id = resumeId
                    graduateData.passing_year = new Date(graduateData.passing_year)
                    await trx('educational_info').where({resume_id : resumeId, "education": "graduation"}).update(graduateData)
                }
                if(EducationalInformation?.postgraduateInfo && EducationalInformation?.postgraduateInfo?.post_college_uni_name !== ''){
                    let postgradData = EducationalInformation.postgraduateInfo
                    let newdata = {
                        education: postgradData.education,
                        college_uni_name: postgradData.post_college_uni_name,
                        marks: postgradData.post_marks,
                        passing_year: new Date(postgradData.post_passing_year),
                        resume_id:resumeId
                    }
                    await trx('educational_info').where({resume_id : resumeId,"education": "postgraduation"}).update(newdata)
                
                }
            }
            
            if(ExperienceInformation){
                let allExperience = await ExperienceInfo.where({resume_id:resumeId}).fetchAll()

                // Find objects present in A but not in B
                const objectsOnlyInA = await getDeletedExp(allExperience,ExperienceInformation)

                console.log("objectsOnlyInA",objectsOnlyInA)
                if(objectsOnlyInA.length){
                    for (let k in objectsOnlyInA){
                        let deletdExp = objectsOnlyInA[k]
                        console.log("deletdExp",deletdExp)
                        const id = deletdExp.id
                        await trx('experience_info').where({ id: id }).del();
                    }
                }
                for (let k in ExperienceInformation){
                    let expInfo = ExperienceInformation[k]
                    const id = expInfo.id
                    delete expInfo['id']

                    expInfo.resume_id = resumeId
                    await trx('experience_info').where({resume_id : resumeId, id: id}).update(expInfo)
                }
                
                // let experiencedata = await ExperienceInfo.where({resume_id: resumeId})
                // console.log(experiencedata);
                // await experiencedata.set({"exp_years": 6})
                
            }

            return res.json({"success":true,"msg":"Updated SuccessFully"})
        }catch(err){
            console.log("Error in createResume",err)
            return res.json({"success":false,
                "error":err})
        }
    }).catch((err)=>{
        console.log("Transaction error while creating resume",err)
    })
} 

const getDeletedExp = async (allExperience,ExperienceInformation)=>{
    const objectsOnlyInA = allExperience.filter((objA) => !ExperienceInformation.some((objB) => objB['id'] === objA['id']));

    return objectsOnlyInA
}
module.exports = {createResume,fetchAllResume,deleteResume, fetchAllTemplates, updateResume}