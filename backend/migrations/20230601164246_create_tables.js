exports.up = function (knex) {
  return knex.schema
    .createTable('users', function (table) {
      table.increments('id').primary();
      table.string('fullName').notNullable();
      table.string('email').notNullable().unique();
      table.string('password').notNullable();
      table.timestamps(true, true);
    })
    .createTable('resumes', function (table) {
      table.increments('id').primary();
      table.integer('user_id').unsigned().notNullable();
      table.foreign('user_id').references('users.id');
      table.timestamps(true, true);
    })
    .createTable('personal_info', function (table) {
      table.increments('id').primary();
      table.string('name').notNullable();
      table.string('email').notNullable();
      table.string('gender');
      table.datetime('dateOfBirth');
      table.string('phoneNumber').notNullable()
      table.integer('resume_id').unsigned().notNullable();
      table.foreign('resume_id').references('resumes.id');
    }).createTable('educational_info', function (table) {
      table.increments('id').primary();
      table.string('education').notNullable();
      table.string('college_uni_name').notNullable();
      table.datetime('passing_year');
      table.integer('marks');        
      table.integer('resume_id').unsigned().notNullable();
      table.foreign('resume_id').references('resumes.id');
    }).createTable('experience_info', function (table) {
      table.increments('id').primary();
      table.string('companies').notNullable();
      table.integer('exp_years').notNullable();
      table.integer('resume_id').unsigned().notNullable();
      table.foreign('resume_id').references('resumes.id');
    })
};

exports.down = function (knex) {
  return knex.schema
    .dropTable('experience_info')  
    .dropTable('educational_info')
    .dropTable('personal_info')
    .dropTable('resumes')
    .dropTable('users');
};
