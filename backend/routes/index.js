const { Router } = require ("express");
const router = Router()
const UserRouter = require ('./User.router.js')
const ResumeRouter = require ('./Resume.router.js')
console.log("here")
router.use('/user',UserRouter)
router.use('/resume',ResumeRouter)
module.exports = router