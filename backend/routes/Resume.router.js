const { Router } = require("express");
const { createResume, fetchAllResume, deleteResume, fetchAllTemplates,updateResume } = require("../controllers/Resume.controller.js");
const auth = require('../middlewares/auth.middleware.js')
const ResumeRouter  = Router()

ResumeRouter.post('/create',auth,createResume)
ResumeRouter.get('/fetchAll',auth,fetchAllResume)
ResumeRouter.delete('/delete/:id',auth,deleteResume)
ResumeRouter.get('/fetchTemplates',fetchAllTemplates)
ResumeRouter.post('/update/:id',auth,updateResume)


module.exports = ResumeRouter;