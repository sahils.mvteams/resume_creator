const { Router } =  require("express");
const { registerUser, loginUser, fetchColleges } = require("../controllers/User.controller.js");
const UserRouter  = Router()

UserRouter.post('/signup',registerUser)
UserRouter.post('/login',loginUser)
UserRouter.get('/fetchColleges',fetchColleges)
module.exports =  UserRouter