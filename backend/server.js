const bodyParser = require("body-parser")
const express = require("express")
const cors = require('cors')
const dotenv = require('dotenv')
dotenv.config()
const routes = require ("./routes/index.js")
const { bookshelfInstance, knexInstance } = require ("./database/index.js")

const app = express()
const port = process.env.PORT || 7000

app.use(express());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

app.use('/api',routes)
app.listen(port,(err)=>{
    if(err) console.log("error while listening to server")

    console.log("Server connected to port ",port)
})

